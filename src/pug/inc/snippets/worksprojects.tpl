
				[!DocLister?
					&display=`2`
					&parents=`[*id*]`
					&id=`page`
					&sortBy=`menuindex`
					&paginate=`pages`
					&tvList=`pageImage`
					&tvPrefix=``
					&prepare=`\ProjectSoft\Prepare::prepareWorks`
					&filters=`[+filters+]`
					&ownerTPL=`@CODE:<div class="worksprojects carousel--watch-css">[+dl.wrap+]</div>`
					&tpl=`@CODE:
						<div class="works--project">
							<div class="works--project-card">
								<a href="[+pageImage+]" data-fancybox="gallery" data-caption="[+caption+]" data-animation-effect="zoom-in-out" data-transition-effect="circular" data-transition-duration="700">
									<img src="[+thumbnail+]" alt="[+caption+]">
								</a>
							</div>
						</div>
					`
					&TplWrapPaginate=`@CODE:
						<div class="pagination">
							<div class="pagination--list" role="navigation">
								[+wrap+]
							</div>
						</div>
					`
					&TplPage=`@CODE:
						<a href="[+link+]">[+num+]</a>
					`
					&TplCurrentPage=`@CODE:
						<span class="current">[+num+]</span>
					`
					&TplNextP=`@CODE:
						<a href="[+link+]">Вперёд</a>
					`
					&TplPrevP=`@CODE:
						<a href="[+link+]">Назад</a>
					`
				!]
				[+page.pages+]