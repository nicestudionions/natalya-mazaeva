[[DocLister?
	&display=`all`
	&parents=`1`
	&sortBy=`menuindex`
	&tvList=`imageCard`
	&documents=`17,16`
	&order=`ASC`
	&tvPrefix=``
	&prepare=`\ProjectSoft\Prepare::prepareHomeWorks`
	&ownerTPL=`@CODE:<div class="section-page-list">[+dl.wrap+]</div>`
	&tpl=`@CODE:
		<article class="project">
			<div class="project--image" style="background-image: url([+thumbnail+]);">
				<a class="link" href="[+url+]">
					<img src="[+thumbnail+]" alt="[+pagetitle:hsc+]">
				</a>
			</div>
			<section class="project--section">
				<header>
					<h2>[+pagetitle+]</h2>
					<div class="project--section--text">
						<p>[+introtext+]</p>
					</div>
				</header>
				<footer>
					<a class="link" href="[+url+]">Подробнее</a>
				</footer>
			</section>
		</article>
	`
]]