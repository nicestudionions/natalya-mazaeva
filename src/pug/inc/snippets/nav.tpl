
			<nav class="navhead" role="navigation">
				<div class="brand">
					<a class="link" href="/">
						<div class="brand-logo">
							<img src="assets/templates/natalya/images/logotip.svg" alt="Логотип Наталья Мазаева">
						</div>
						<div class="brand-text">
							<span class="brand-text-up">Наталья Мазаева</span>
							<span class="brand-text-down">Флорист - декоратор</span>
						</div>
					</a>
				</div>
				<div class="navbar">
					<ul class="navbar-list">
						<li class="navbar-list-li">
							<a class="link" href="[~15~]">Обо мне</a>
						</li>
						<li class="navbar-list-li">
							<a class="link" href="[~16~]">Работы</a>
						</li>
						<li class="navbar-list-li">
							<a class="link" href="[~18~]">Контакты</a>
						</li>
					</ul>
				</div>
			</nav>