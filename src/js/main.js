(function($){
	var onInit = function(){
			$('.worksprojects').flickity({
				cellAlign: 'center',
				setGallerySize: true,
				wrapAround: true,
				contain: true,
				prevNextButtons: false,
				pageDots: false,
				watchCSS: true
			});
			$(window).trigger("resize");
		},
		onLoadAjax = function(link){
			var $header = $("#header .header-block"),
				$section = $("#sectionpage");
			$.ajax({
				url: link,
				dataType: 'html',
				success: function(data) {
					var pdata = $.parseHTML(data),
						html = $($.parseHTML(data)).filter("#mainpage"),
						title = $($.parseHTML(data)).filter("title"),
						header = $("#header .header-block", html).html(),
						section = $("#sectionpage", html).html();
					document.title = title.text();
					$header.html(header);
					$section.html(section);
					onInit();
					setTimeout(function(){
						$("body").removeClass("load");
					}, 10);
				},
				error: function(){
					$("body").removeClass("load");
				}
			});
		};
	History.Adapter.bind(window, 'statechange',function(e){
		var State = History.getState();
		$("body").addClass("load");
		/*$("html, body").stop().animate({
			scrollTop:0
		}, 200, 'swing');*/
		setTimeout(function(){
			onLoadAjax(State.url);
		}, 10);
	});
	$("#mainpage").on('click', "#sidebar a, .pagination a, a.link, .worksnav a", function(e){
		e.preventDefault();
		History.pushState(null, document.title, $(this).attr('href'));
		return !1;
	});
	
	setTimeout(function(){
		onInit();
	}, 100);
}(jQuery));