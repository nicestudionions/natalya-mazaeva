module.exports = function(grunt) {
	var gc = {
		fontVers: '1.0.0',
		template: 'assets/templates/natalya/',
		tasks: [
			'notify:watch',
			'concat',
			'jshint',
			'uglify',
			'imagemin',
			'tinyimg',
			'webfont',
			'less',
			'autoprefixer',
			'group_css_media_queries',
			'replace',
			'cssmin',
			'newer:copy',
			'pug',
			'notify:done'
		]
	};
	require('load-grunt-tasks')(grunt);
	require('time-grunt')(grunt);
	grunt.initConfig({
		globalConfig : gc,
		pkg : grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: "\n",
			},
			appjs: {
				src: [
					'bower_components/jquery/dist/jquery.js',
					'bower_components/flickity/dist/flickity.pkgd.js',
					'bower_components/fancybox/dist/jquery.fancybox.js',
					//'bower_components/jquery-pjax/jquery.pjax.js',
					'bower_components/history.js/scripts/bundled/html4+html5/jquery.history.js',
				],
				dest: 'test/js/appjs.js'
			},
			main: {
				src: [
					'src/js/main.js'
				],
				dest: 'test/js/main.js',
			},
		},
		jshint: {
			options: {
				expr: true,
				jshintrc: true,
				scripturl: true
			},
			src: [
				'src/js/*.js'
			]
		},
		uglify: {
			app: {
				options: {
					sourceMap: false,
					compress: true
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'test/js/appjs.js'
						],
						dest: '<%= globalConfig.template %>/js/',
						filter: 'isFile'
					},
					{
						expand: true,
						flatten : true,
						src: [
							'test/js/appjs.js'
						],
						dest: '../evo.natalya.my/<%= globalConfig.template %>/js/',
						filter: 'isFile'
					}
				]
			},
			js: {
				options: {
					sourceMap: false,
					compress: true
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'test/js/main.js'
						],
						dest: '<%= globalConfig.template %>/js/',
						filter: 'isFile'
					},
					{
						expand: true,
						flatten : true,
						src: [
							'test/js/main.js'
						],
						dest: '../evo.natalya.my/<%= globalConfig.template %>/js/',
						filter: 'isFile'
					}
				]
			}
		},
		imagemin: {
			base: {
				options: {
					optimizationLevel: 3,
					svgoPlugins: [
						{
							removeViewBox: false
						}
					]
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'src/images/*.{png,jpg,gif,svg}'
						],
						dest: 'test/images/',
						filter: 'isFile'
					}
				],
			}
		},
		tinyimg: {
			dynamic: {
				files: [
					{
						expand: true,
						cwd: 'test/images', 
						src: ['**/*.{png,jpg,jpeg,svg}'],
						dest: '<%= globalConfig.template %>/images/'
					},
					{
						expand: true,
						cwd: 'test/images', 
						src: ['**/*.{png,jpg,jpeg,svg}'],
						dest: '../evo.natalya.my/<%= globalConfig.template %>/images/'
					}
				]
			}
		},
		webfont: {
			icons: {
				src: 'src/glyph/*.svg',
				dest: '<%= globalConfig.template %>/fonts',
				options: {
					hashes: true,
					relativeFontPath: '<%= globalConfig.template %>/fonts/',
					destLess: 'src/less/fonts',
					font: 'natalya',
					types: 'eot,woff2,woff,ttf',
					fontFamilyName: 'Natalya Mazaeva',
					stylesheets: ['less'],
					syntax: 'bootstrap',
					execMaxBuffer: 1024 * 400,
					htmlDemo: false,
					version: gc.fontVers,
					normalize: true,
					startCodepoint: 0xE900,
					iconsStyles: false,
					templateOptions: {
						baseClass: '',
						classPrefix: 'webicon-'
					},
					embed: false,
					template: 'src/less/font-build.template'
				}
			},
			filetypes: {
				src: 'src/glyph/file-types/*.svg',
				dest: '<%= globalConfig.template %>/fonts',
				options: {
					hashes: true,
					relativeFontPath: '/<%= globalConfig.template %>/fonts/',
					destLess: 'src/less/fonts',
					font: 'filetypes',
					types: 'eot,woff2,woff,ttf',
					fontFamilyName: 'File Types',
					stylesheets: ['less'],
					syntax: 'bootstrap',
					execMaxBuffer: 1024 * 400,
					htmlDemo: false,
					version: gc.fontVers,
					normalize: true,
					startCodepoint: 0xE900,
					iconsStyles: false,
					templateOptions: {
						baseClass: '',
						classPrefix: 'filetype-'
					},
					embed: false,
					template: 'src/less/font-build.template'
				}
			},
			relicons: {
				src: 'src/glyph/*.svg',
				dest: '../evo.natalya.my/<%= globalConfig.template %>/fonts',
				options: {
					hashes: true,
					relativeFontPath: '<%= globalConfig.template %>/fonts/',
					destLess: 'src/less/fonts',
					font: 'natalya',
					types: 'eot,woff2,woff,ttf',
					fontFamilyName: 'Natalya Mazaeva',
					stylesheets: ['less'],
					syntax: 'bootstrap',
					execMaxBuffer: 1024 * 400,
					htmlDemo: false,
					version: gc.fontVers,
					normalize: true,
					startCodepoint: 0xE900,
					iconsStyles: false,
					templateOptions: {
						baseClass: '',
						classPrefix: 'webicon-'
					},
					embed: false,
					template: 'src/less/font-build.template'
				}
			},
			relfiletypes: {
				src: 'src/glyph/file-types/*.svg',
				dest: '../evo.natalya.my/<%= globalConfig.template %>/fonts',
				options: {
					hashes: true,
					relativeFontPath: '/<%= globalConfig.template %>/fonts/',
					destLess: 'src/less/fonts',
					font: 'filetypes',
					types: 'eot,woff2,woff,ttf',
					fontFamilyName: 'File Types',
					stylesheets: ['less'],
					syntax: 'bootstrap',
					execMaxBuffer: 1024 * 400,
					htmlDemo: false,
					version: gc.fontVers,
					normalize: true,
					startCodepoint: 0xE900,
					iconsStyles: false,
					templateOptions: {
						baseClass: '',
						classPrefix: 'filetype-'
					},
					embed: false,
					template: 'src/less/font-build.template'
				}
			}
		},
		less: {
			css: {
				options : {
					compress: true,
					ieCompat: false
				},
				files : {
					'test/css/main.css' : [
						'src/less/main.less'
					]
				}
			}
		},
		autoprefixer:{
			options: {
				browsers: ['last 2 versions', 'Android 4', 'ie 8', 'ie 9', 'Firefox >= 27', 'Opera >= 12.0', 'Safari >= 6'],
				cascade: true
			},
			css: {
				files: {
					'test/css/prefix/main.css' : ['test/css/main.css']
				}
			},
		},
		group_css_media_queries: {
			group: {
				files: {
					'test/css/media/main.css': ['test/css/prefix/main.css']
				}
			}
		},
		replace: {
			dist: {
				options: {
					patterns: [
						{
							match: /\/\* *(.*?) *\*\//g,
							replacement: ' '
						}
					]
				},
				files: [
					{
						expand: true,
						flatten : true,
						src: [
							'test/css/media/*.css'
						],
						dest: 'test/css/replace/',
						filter: 'isFile'
					}
				]
			}
		},
		cssmin: {
			options: {
				mergeIntoShorthands: false,
				roundingPrecision: -1
			},
			minify: {
				files: {
					'<%= globalConfig.template %>/css/main.css' : ['test/css/replace/main.css'],
					'../evo.natalya.my/<%= globalConfig.template %>/css/main.css' : ['test/css/replace/main.css'],
				}
			}
		},
		pug: {
			files: {
				options: {
					pretty: '\t',
					separator:  '\n'
				},
				files: {
					"index.html": ['src/pug/index.pug'],
					"about.html": ['src/pug/about.pug'],
					"works.html": ['src/pug/works.pug'],
					"contacts.html": ['src/pug/contacts.pug'],
					"<%= globalConfig.template %>/index.html": ['src/pug/index-template.pug'],
					"<%= globalConfig.template %>/about.html": ['src/pug/about-template.pug'],
					"<%= globalConfig.template %>/works.html": ['src/pug/works-template.pug'],
					"<%= globalConfig.template %>/contacts.html": ['src/pug/contacts-template.pug'],
					// Сразу на локальный сервер
					"../evo.natalya.my/<%= globalConfig.template %>/index.html": ['src/pug/index-template.pug'],
					"../evo.natalya.my/<%= globalConfig.template %>/about.html": ['src/pug/about-template.pug'],
					"../evo.natalya.my/<%= globalConfig.template %>/works.html": ['src/pug/works-template.pug'],
					"../evo.natalya.my/<%= globalConfig.template %>/contacts.html": ['src/pug/contacts-template.pug']
				}
			}
		},
		copy: {
			fonts: {
				expand: true,
				cwd: 'src/fonts',
				src: [
					'**.{ttf,svg,eot,woff,woff2}'
				],
				dest: '<%= globalConfig.template %>/fonts/',
			},
			releace: {
				expand: true,
				cwd: 'src/fonts',
				src: [
					'**.{ttf,svg,eot,woff,woff2}'
				],
				dest: '../evo.natalya.my/<%= globalConfig.template %>/fonts/',
			}
		},
		notify: {
			watch: {
				options: {
					title: "<%= pkg.name %> v<%= pkg.version %>",
					message: 'Запуск',
					image: __dirname+'\\src\\notify.png'
				}
			},
			done: {
				options: { 
					title: "<%= pkg.name %> v<%= pkg.version %>",
					message: "Успешно Завершено",
					image: __dirname+'\\src\\notify.png'
				}
			}
		},
		watch: {
			options: {
				livereload: true,
			},
			compile: {
				files: [
					'src/**/*.*'
				],
				tasks: gc.tasks
			}
		}
	});
	grunt.registerTask('dev',		['watch']);
	grunt.registerTask('default',	gc.tasks);
	grunt.registerTask('css',	[
			'notify:watch',
			'webfont',
			'less',
			'autoprefixer',
			'group_css_media_queries',
			'replace',
			'cssmin',
			'newer:copy',
			'notify:done'
		]);
};